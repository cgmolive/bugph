const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.titulo) return util.bind(new Error('Enter your title!'))
    if (!body.imagem) return util.bind(new Error('Insert something in your post!'))

    const insert = await mysql.query('insert into Posts (titulo, imagem, datahora) values (?,?,?)', [body.titulo, body.imagem,sysdate])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}