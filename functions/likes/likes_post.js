const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const insert = await mysql.query('insert into Likes (datahora) values (?)', [sysdate])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}