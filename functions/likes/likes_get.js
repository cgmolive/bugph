const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    
    if (event.pathParameters && event.pathParameters.id) {
      const like = await mysql.query('select id, id_usuario, datahora from likes where id=?', [event.pathParameters.id])
      return util.bind(like.length ? log[0] : {})
    }

    const post = await mysql.query('select id, id_usuario, id_post, datahora from likes')
    return util.bind(logs)
  } catch (error) {
    return util.bind(error)
  }
}
