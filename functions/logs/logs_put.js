const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.id) return util.bind(new Error('Enter the log code!'))

    await mysql.query('update logs set log=? where id=?', [body.log, body.id])
    return util.bind({})
  } catch (error) {
    return util.bind(error)
  }
}
