const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.log) return util.bind(new Error('Enter the log!'))


    const insert = await mysql.query('insert into logs (log, datahora) values (?,?)', [body.log, sysdate])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}
