const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    
    if (event.pathParameters && event.pathParameters.id) {
      const log = await mysql.query('select id, id_usuario, log, datahora from logs where id=?', [event.pathParameters.id])
      return util.bind(user.length ? log[0] : {})
    }

    const logs = await mysql.query('select id, id_usuario, log, datahora from logs')
    return util.bind(logs)
  } catch (error) {
    return util.bind(error)
  }
}
