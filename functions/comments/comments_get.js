const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    
    if (event.pathParameters && event.pathParameters.id) {
      const comment = await mysql.query('select id, id_usuario, comentario, datahora from comments where id=?', [event.pathParameters.id])
      return util.bind(comment.length ? comment[0] : {})
    }

    const comments = await mysql.query('select id, id_usuario, comentario, datahora from comments')
    return util.bind(comments)
  } catch (error) {
    return util.bind(error)
  }
}
