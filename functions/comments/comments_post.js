const mysql = require('../../lib/mysql')
const util = require('../../lib/util')

module.exports = async (event) => {
  try {
    const body = JSON.parse(event.body || '{}')
    if (!body.comentario) return util.bind(new Error('Enter your comment!'))

    const insert = await mysql.query('insert into comments (comentario,datahora) values (?,?,?)', [body.comentario,sysdate])
    return util.bind(insert)
  } catch (error) {
    return util.bind(error)
  }
}