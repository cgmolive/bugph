const get = require('./comments_get.js')
const post = require('./comments_post.js')

module.exports = {
  get,
  post,
}
